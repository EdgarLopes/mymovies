//
//  main.m
//  MyMovies
//
//  Created by Edgar Filipe Batista Lopes on 24/06/15.
//  Copyright (c) 2015 BatistaLopes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
