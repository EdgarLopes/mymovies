//
//  WebServiceClient.m
//  MyMovies
//
//  Created by Edgar Filipe Batista Lopes on 24/06/15.
//  Copyright (c) 2015 BatistaLopes. All rights reserved.
//

#import "WebServiceClient.h"


@implementation WebServiceClient


static WebServiceClient *_sharedWebServiceAccess = nil;

+(WebServiceClient*)sharedWebServiceAccess{

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedWebServiceAccess = [[self alloc] initWithBaseURL:[NSURL URLWithString:API_HOST_URL]];
    });
    
    return _sharedWebServiceAccess;
    
}


-(void)GET:(NSString *)service  parameters:(NSDictionary *)parameters responseBlock:(ConnectionResponse)block{
    
    
    if ([service rangeOfString:@":id"].location != NSNotFound) {
        NSParameterAssert(parameters[@"id"]);
        service = [service stringByReplacingOccurrencesOfString:@":id" withString:parameters[@"id"]];
    }
    
    NSMutableDictionary *params = parameters ? [parameters mutableCopy] : [NSMutableDictionary new];
    params[@"api_key"] = API_KEY;
    
    [self GET:service parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        block(responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        block(nil, error);
    }];
    
}

@end
