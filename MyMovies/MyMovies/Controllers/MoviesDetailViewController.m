//
//  MoviesDetailViewController.m
//  MyMovies
//
//  Created by Edgar Filipe Batista Lopes on 09/05/15.
//  Copyright (c) 2015 BL. All rights reserved.
//

#import "MoviesDetailViewController.h"
#import <UIImageView+AFNetworking.h>



@interface MoviesDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *moviePoster;
@property (weak, nonatomic) IBOutlet UILabel *movieTitle;
@property (weak, nonatomic) IBOutlet UILabel *movieTagLine;
@property (weak, nonatomic) IBOutlet UILabel *movieProduction;
@property (weak, nonatomic) IBOutlet UILabel *movieDuration;
@property (weak, nonatomic) IBOutlet UILabel *movieSite;
@property (weak, nonatomic) IBOutlet UITextView *movieOverview;

@end

@implementation MoviesDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    NSLog(@"Object %@", _movieDetail);
    
    _movieTitle.text = _movieDetail[@"original_title"];
    
    _movieTagLine.text = _movieDetail[@"tagline"];
    
    [_moviePoster setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", _imagesBaseURL, _movieDetail[@"poster_path"]]]];
    
    NSString *prod_companies = [[_movieDetail[@"production_companies"] valueForKey:@"name"] componentsJoinedByString:@", "];
    _movieProduction.text = prod_companies;
    
    _movieDuration.text = [@"Runtime: " stringByAppendingString:[_movieDetail[@"runtime"] stringValue]];
    
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_movieDetail[@"homepage"] attributes:nil];
    NSRange linkRange = NSMakeRange(0, [_movieDetail[@"homepage"] length]);
    
    NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:0.05 green:0.4 blue:0.65 alpha:1.0],
                                      NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle) };
    [attributedString setAttributes:linkAttributes range:linkRange];
    
    _movieSite.attributedText = attributedString;
    
    

    _movieSite.userInteractionEnabled = YES;
    [_movieSite addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)]];
    
    _movieOverview.text = _movieDetail[@"overview"];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [_movieOverview setContentOffset:CGPointZero animated:NO];
}

- (void)handleTapOnLabel:(UITapGestureRecognizer *)tapGesture
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_movieSite.text]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
