//
//  MoviesTableViewController.m
//  MyMovies
//
//  Created by Edgar Filipe Batista Lopes on 07/05/15.
//  Copyright (c) 2015 BL. All rights reserved.
//

#import "MoviesTableViewController.h"
#import "WebServiceClient.h"
#import <UIImageView+AFNetworking.h>
#import "MoviesDetailViewController.h"

@interface MoviesTableViewController (){

    NSMutableArray *movies;     // Data source of table view.
    
    NSString *imagesBaseURL92;  // Base URL of images. This string can be obtained on the API /configuration method
    
    NSString *imagesBaseURL300; // Base URL of images to be loaded on MoviesDetailViewController.h
    
    NSNumber *currentPage;      // Current page to be used on the API method /movie/popular. Should be updated each time the API mehtod is invoked
}



@end

@implementation MoviesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 150;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    // Refresh control to refresh movie list
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.tintColor = [UIColor grayColor];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refresh addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refresh];
    self.refreshControl = refresh;
    
    // Data source
    movies = [[NSMutableArray alloc] init];

    
    // Get configurations
    
    [[WebServiceClient sharedWebServiceAccess] GET:API_CONFIGURATION parameters:nil responseBlock:^(id response, NSError *error) {
        
        
        if (error == NULL) {
            
            imagesBaseURL92 = [response[@"images"][@"base_url"] stringByAppendingString:@"w92"];
            
            imagesBaseURL300 = [response[@"images"][@"base_url"] stringByAppendingString:@"w300"];
            
            //Get first page
            
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            
            parameters[@"page"] = @(1);
            
            [[WebServiceClient sharedWebServiceAccess] GET:API_POPULAR_MOVIES parameters:parameters responseBlock:^(id response, NSError *error) {
                
                if(error == NULL){
                    
                    currentPage = response[@"page"];
                    
                    for(id object in response[@"results"])
                        [movies addObject:[NSMutableDictionary dictionaryWithDictionary:object]];
                    
                    
                    [self.tableView reloadData];
                    
                    
                }
                
                
            }];
            
        }
    }];
    
    



 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Refresh control
// Refresh control. Loads objects from 1st page.
-(void)refresh{

    
    [movies removeAllObjects];
    
    
    
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    parameters[@"page"] = @(1);
    
    [[WebServiceClient sharedWebServiceAccess] GET:API_POPULAR_MOVIES parameters:parameters responseBlock:^(id response, NSError *error) {
        
        if (error == NULL) {
            
            currentPage = response[@"page"];
            
            for(id object in response[@"results"])
                [movies addObject:[NSMutableDictionary dictionaryWithDictionary:object]];
            
            
            [self.tableView reloadData];
            
            [self.refreshControl endRefreshing];
            
            
            UIView *footer = [self.tableView viewWithTag:8];
            if(footer.hidden == YES) footer.hidden = NO;
            
        }
        
       
        
    }];
    
    


}


#pragma mark - Next page
// Footer action. Load popular movies from currentPage + 1;
- (IBAction)loadNextPage:(id)sender {
    
    
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    parameters[@"page"] = @(currentPage.intValue +1);
    
    [[WebServiceClient sharedWebServiceAccess] GET:API_POPULAR_MOVIES parameters:parameters  responseBlock:^(id response, NSError *error) {
        
       
        currentPage = response[@"page"];
        
        for(id object in response[@"results"]){
            
            [movies addObject:[NSMutableDictionary dictionaryWithDictionary:object]];
        }
        
        [self.tableView reloadData];
        
        
        //API only loads 1000 pages. If current page >= 1001, then the footer view should be hidden to avoid calling the API method 'movie/popular' with invalid parameters
        
        if (currentPage.intValue >= 1001) {
            
            UIView *footer = [self.tableView viewWithTag:8];
            footer.hidden = YES;
        }
        
    
        
        
    }];

}




#pragma mark - Table view data source



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return movies.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellMovies" forIndexPath:indexPath];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellMovies"];
        
    }
    
    NSMutableDictionary *data = movies[indexPath.row];
    
  
    
    UILabel *title = (UILabel *)[cell.contentView viewWithTag:1];

    title.text = data[@"original_title"];
    
    
    
    UILabel *releaseDate = (UILabel *)[cell.contentView viewWithTag:2];
    
    releaseDate.text = data[@"release_date"];
    
    
    UILabel *popularity = (UILabel *)[cell.contentView viewWithTag:5];
    
    popularity.text = [NSString stringWithFormat:@"%.2f", [data[@"popularity"] doubleValue]];
    

    UIImageView *thumb = (UIImageView *)[cell.contentView viewWithTag:3];
   
    //Lazy loading images using UIImageView+AFNtworking
    if (![data[@"poster_path"] isKindOfClass:[NSNull class]]) {
        [thumb setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", imagesBaseURL92, data[@"poster_path"]]]];
    }
    
    UILabel *genre = (UILabel *)[cell.contentView viewWithTag:4];
    
    
    // Check if "detail" exists on ou data source item. If not, get details using a background thread.
    if (data[@"detail"] == nil) {
     
        // Get genre on a background thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            
            parameters[@"id"] = [NSString stringWithFormat:@"%@",data[@"id"]];
            
            [[WebServiceClient sharedWebServiceAccess] GET:API_MOVIE_BASIC_INFO parameters:parameters responseBlock:^(id response, NSError *error) {
                
                if (error == NULL) {
                    
                    data[@"detail"] = response;
                    
                    [movies replaceObjectAtIndex:indexPath.row withObject:data];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSString *genreStr = [[response[@"genres"] valueForKey:@"name"] componentsJoinedByString:@", "];
                        
                        genre.text = genreStr;
                    });

                }

            }];
            
            
        });
  

        
    }else{
    
        NSString *genreStr = [[data[@"detail"][@"genres"] valueForKey:@"name"] componentsJoinedByString:@", "];
        
        genre.text = genreStr;
        
    }
    
    
    return cell;
}









#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"showMovieDetail"]) {
        MoviesDetailViewController *movieDetailVC = segue.destinationViewController;
        movieDetailVC.movieDetail = movies[self.tableView.indexPathForSelectedRow.row][@"detail"];
        movieDetailVC.imagesBaseURL = imagesBaseURL300;
    }
    
}


@end
