//
//  MoviesDetailViewController.h
//  MyMovies
//
//  Created by Edgar Filipe Batista Lopes on 09/05/15.
//  Copyright (c) 2015 BL. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface MoviesDetailViewController : UIViewController

@property (strong) NSDictionary *movieDetail;

@property (nonatomic, strong)  NSString *imagesBaseURL;

@end
