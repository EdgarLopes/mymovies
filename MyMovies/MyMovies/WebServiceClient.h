//
//  WebServiceClient.h
//  MyMovies
//
//  Created by Edgar Filipe Batista Lopes on 24/06/15.
//  Copyright (c) 2015 BatistaLopes. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"
#import "Constants.h"


typedef void (^ConnectionResponse)(id response, NSError *error);  // Response block of a connection service. It returns a reponse object or a error;

@interface WebServiceClient : AFHTTPRequestOperationManager

+(WebServiceClient*)sharedWebServiceAccess;

-(void)GET:(NSString *)service  parameters:(NSDictionary *)parameters responseBlock:(ConnectionResponse)block;

@end
