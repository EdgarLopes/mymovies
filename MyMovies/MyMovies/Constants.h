//
//  Constants.h
//  MyMovies
//
//  Created by Edgar Filipe Batista Lopes on 08/05/15.
//  Copyright (c) 2015 BL. All rights reserved.
//

#pragma mark - Movies API

//API_KEY
static NSString * const API_KEY = @"d5af30d2592907b31082a834f8fac860";

//Host URL
static NSString * const API_HOST_URL = @"http://api.themoviedb.org/3/";


static NSString * const API_CONFIGURATION = @"configuration";

// Popular movies List
static NSString * const API_POPULAR_MOVIES = @"movie/popular";

//BASIC
static NSString * const API_MOVIE_BASIC_INFO = @"movie/:id";
